#include<stddef.h>
#include<stdint.h>
#include"vid.h"
#include"../sys.h"

static uint8_t *frameBuffer;

static size_t commandIndex = 0;
static uint8_t *commandBuffer;

static size_t imageIDNext = 0;
static uint8_t **imageIDsBuffer;

char stack[768];

void select_planes(int planes) {
	asm volatile("out %%al, %%dx" : : "a"(2), "d"(0x3C4));
	asm volatile("out %%al, %%dx" : : "a"(planes), "d"(0x3C5));
}

void clear(uint8_t col) {
	select_planes(15);
	for(int i = 0; i < 4096; i++) {
		((uint32_t*) frameBuffer)[i] = col | (col << 8) | (col << 16) | (col << 24);
	}
}

void rect(int col, int x1, int y1, int x2, int y2) {
	select_planes(1 << ((x1 + 0) & 3));
	for(int x = x1 + 0; x <= x2; x += 4) {
		for(int y = y1; y <= y2; y++) {
			frameBuffer[80 * y + x / 4] = col;
		}
	}
	select_planes(1 << ((x1 + 1) & 3));
	for(int x = x1 + 1; x <= x2; x += 4) {
		for(int y = y1; y <= y2; y++) {
			frameBuffer[80 * y + x / 4] = col;
		}
	}
	select_planes(1 << ((x1 + 2) & 3));
	for(int x = x1 + 2; x <= x2; x += 4) {
		for(int y = y1; y <= y2; y++) {
			frameBuffer[80 * y + x / 4] = col;
		}
	}
	select_planes(1 << ((x1 + 3) & 3));
	for(int x = x1 + 3; x <= x2; x += 4) {
		for(int y = y1; y <= y2; y++) {
			frameBuffer[80 * y + x / 4] = col;
		}
	}
}

void image(uint8_t *img, uint16_t x, uint16_t y) {
	int w = ((uint16_t*) img)[0];
	int h = ((uint16_t*) img)[1];
	img += 4;
	for(int ix = 0; ix < w; ix++) {
		select_planes(1 << ((x + ix) & 3));
		for(int iy = 0; iy < h; iy++) {
			uint8_t v = img[iy * w + ix];
			if(v) {
				frameBuffer[80 * (iy + y) + (ix + x) / 4] = v;
			}
		}
	}
}

static int grad(int x, int y) {
	static uint8_t matrix[16][16] = {{0, 64, 16, 80, 4, 68, 20, 84, 1, 65, 17, 81, 5, 69, 21, 85}, {192, 128, 208, 144, 196, 132, 212, 148, 193, 129, 209, 145, 197, 133, 213, 149}, {48, 112, 32, 96, 52, 116, 36, 100, 49, 113, 33, 97, 53, 117, 37, 101}, {240, 176, 224, 160, 244, 180, 228, 164, 241, 177, 225, 161, 245, 181, 229, 165}, {12, 76, 28, 92, 8, 72, 24, 88, 13, 77, 29, 93, 9, 73, 25, 89}, {204, 140, 220, 156, 200, 136, 216, 152, 205, 141, 221, 157, 201, 137, 217, 153}, {60, 124, 44, 108, 56, 120, 40, 104, 61, 125, 45, 109, 57, 121, 41, 105}, {252, 188, 236, 172, 248, 184, 232, 168, 253, 189, 237, 173, 249, 185, 233, 169}, {3, 67, 19, 83, 7, 71, 23, 87, 2, 66, 18, 82, 6, 70, 22, 86}, {195, 131, 211, 147, 199, 135, 215, 151, 194, 130, 210, 146, 198, 134, 214, 150}, {51, 115, 35, 99, 55, 119, 39, 103, 50, 114, 34, 98, 54, 118, 38, 102}, {243, 179, 227, 163, 247, 183, 231, 167, 242, 178, 226, 162, 246, 182, 230, 166}, {15, 79, 31, 95, 11, 75, 27, 91, 14, 78, 30, 94, 10, 74, 26, 90}, {207, 143, 223, 159, 203, 139, 219, 155, 206, 142, 222, 158, 202, 138, 218, 154}, {63, 127, 47, 111, 59, 123, 43, 107, 62, 126, 46, 110, 58, 122, 42, 106}, {255, 191, 239, 175, 251, 187, 235, 171, 254, 190, 238, 174, 250, 186, 234, 170}};
	
	int c = 80 + x / 40;
	return c + 255 * ((int) matrix[x % 16][y % 16] - 128) / 30 / 256;
}

static void (*compileCommandList(volatile uint8_t *src))() {
	void *ret = &commandBuffer[commandIndex];
	
	while(1) {
		volatile LumaVidCommand *cmd = (void*) src;
		
		if(cmd->op == LUMA_VID_COMMAND_CLEAR) {
			commandBuffer[commandIndex++] = 0x6A; /* push imm8 */
			commandBuffer[commandIndex++] = cmd->clear.color;
			commandBuffer[commandIndex++] = 0xE8; /* call rel32 */
			*((uint32_t*) &commandBuffer[commandIndex]) = (uint32_t) &clear - (uint32_t) &commandBuffer[commandIndex + 4];
			commandIndex += 4;
			commandBuffer[commandIndex++] = 0x83; /* add */
			commandBuffer[commandIndex++] = 0xC4; /* esp */
			commandBuffer[commandIndex++] = 0x04; /* 4 */
			src += sizeof(cmd->clear);
		} else if(cmd->op == LUMA_VID_COMMAND_END) {
			commandBuffer[commandIndex++] = 0xC3; /* ret */
			break;
		} else if(cmd->op == LUMA_VID_COMMAND_RECT) {
			commandBuffer[commandIndex++] = 0x68; /* push imm32 */
			commandBuffer[commandIndex++] = cmd->rect.y2 & 0xFF;
			commandBuffer[commandIndex++] = cmd->rect.y2 >> 8;
			commandBuffer[commandIndex++] = 0;
			commandBuffer[commandIndex++] = 0;
			commandBuffer[commandIndex++] = 0x68; /* push imm32 */
			commandBuffer[commandIndex++] = cmd->rect.x2 & 0xFF;
			commandBuffer[commandIndex++] = cmd->rect.x2 >> 8;
			commandBuffer[commandIndex++] = 0;
			commandBuffer[commandIndex++] = 0;
			commandBuffer[commandIndex++] = 0x68; /* push imm32 */
			commandBuffer[commandIndex++] = cmd->rect.y1 & 0xFF;
			commandBuffer[commandIndex++] = cmd->rect.y1 >> 8;
			commandBuffer[commandIndex++] = 0;
			commandBuffer[commandIndex++] = 0;
			commandBuffer[commandIndex++] = 0x68; /* push imm32 */
			commandBuffer[commandIndex++] = cmd->rect.x1 & 0xFF;
			commandBuffer[commandIndex++] = cmd->rect.x1 >> 8;
			commandBuffer[commandIndex++] = 0;
			commandBuffer[commandIndex++] = 0;
			commandBuffer[commandIndex++] = 0x6A; /* push imm8 */
			commandBuffer[commandIndex++] = cmd->rect.color;
			commandBuffer[commandIndex++] = 0xE8; /* call rel32 */
			*((uint32_t*) &commandBuffer[commandIndex]) = (uint32_t) &rect - (uint32_t) &commandBuffer[commandIndex + 4];
			commandIndex += 4;
			commandBuffer[commandIndex++] = 0x83; /* add */
			commandBuffer[commandIndex++] = 0xC4; /* esp */
			commandBuffer[commandIndex++] = 20;
			src += sizeof(cmd->rect);
		} else if(cmd->op == LUMA_VID_COMMAND_IMAGE) {
			commandBuffer[commandIndex++] = 0x68; /* push imm32 */
			commandBuffer[commandIndex++] = cmd->image.y & 0xFF;
			commandBuffer[commandIndex++] = cmd->image.y >> 8;
			commandBuffer[commandIndex++] = 0;
			commandBuffer[commandIndex++] = 0;
			commandBuffer[commandIndex++] = 0x68; /* push imm32 */
			commandBuffer[commandIndex++] = cmd->image.x & 0xFF;
			commandBuffer[commandIndex++] = cmd->image.x >> 8;
			commandBuffer[commandIndex++] = 0;
			commandBuffer[commandIndex++] = 0;
			commandBuffer[commandIndex++] = 0x68; /* push imm32 */
			uint8_t *img = imageIDsBuffer[cmd->image.imgID];
			commandBuffer[commandIndex++] = ((uint32_t) img >> 0) & 0xFF;
			commandBuffer[commandIndex++] = ((uint32_t) img >> 8) & 0xFF;
			commandBuffer[commandIndex++] = ((uint32_t) img >> 16) & 0xFF;
			commandBuffer[commandIndex++] = ((uint32_t) img >> 24) & 0xFF;
			commandBuffer[commandIndex++] = 0xE8; /* call rel32 */
			*((uint32_t*) &commandBuffer[commandIndex]) = (uint32_t) &image - (uint32_t) &commandBuffer[commandIndex + 4];
			commandIndex += 4;
			commandBuffer[commandIndex++] = 0x83; /* add */
			commandBuffer[commandIndex++] = 0xC4; /* esp */
			commandBuffer[commandIndex++] = 12;
			src += sizeof(cmd->image);
		} else if(cmd->op == LUMA_VID_COMMAND_IMAGE_NEW) {
			size_t len = cmd->imageNew.width * cmd->imageNew.height + 4;
			volatile uint8_t *buf = (void*) syscall(0, -1, -1, len);
			for(size_t i = 0; i < len; i++) {
				buf[i] = src[1 + i];
			}
			imageIDsBuffer[imageIDNext++] = buf;
			src += sizeof(cmd->imageNew) + cmd->imageNew.width * cmd->imageNew.height;
		}
	}
	
	return (void(*)()) ret;
}

void startc() {
	frameBuffer = sys_vpm_map(-1, 0xA0000, 16384);
	commandBuffer = sys_vpm_map(-1, -1, 4096);
	imageIDsBuffer = sys_vpm_map(-1, -1, 4096);
	
	uint32_t canalID = sys_canal_create(0x0B140C00, 0x1E150803);
	
	for(int p = 0; p < 4; p++) {
		select_planes(1 << p);
		for(int x = 0; x < 80; x++) {
			for(int y = 0; y < 200; y++) {
				frameBuffer[y * 80 + x] = grad(x * 4 + p, y);
			}
		}
	}
	
	uint8_t *buf;
	do {
		buf = sys_canal_accept(canalID);
	} while(buf == -1);
	
	while(1) {
		sys_signal_wait();
		
		memory_barrier();
		
		void(*asdf)() = compileCommandList(buf);
		asdf();
		
		sys_signal_send(buf);
	}
}