#ifndef _LUMA_VBE_VBE_H
#define _LUMA_VBE_VBE_H

#include<stdint.h>

typedef struct {
	uint16_t width;
	uint16_t height;
	uint8_t data[];
} __attribute__((packed)) LumaVidImage;

typedef enum {
	LUMA_VID_COMMAND_END = 0, LUMA_VID_COMMAND_CLEAR = 1, LUMA_VID_COMMAND_RECT = 2, LUMA_VID_COMMAND_IMAGE = 3, LUMA_VID_COMMAND_IMAGE_NEW = 4
} LumaVidCommandOp;
typedef union {
	uint8_t op;
	struct {
		uint8_t op;
		uint8_t color;
	} __attribute__((packed)) clear;
	struct {
		uint8_t op;
		uint16_t y2;
		uint16_t x2;
		uint16_t y1;
		uint16_t x1;
		uint8_t color;
	} __attribute__((packed)) rect;
	struct {
		uint8_t op;
		uint16_t y;
		uint16_t x;
		uint32_t imgID;
	} __attribute__((packed)) image;
	struct {
		uint8_t op;
		uint16_t width;
		uint16_t height;
		uint8_t data[];
	} __attribute__((packed)) imageNew;
} __attribute__((packed)) LumaVidCommand;

#endif