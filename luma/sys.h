#ifndef _LUMA_SYS_H
#define _LUMA_SYS_H

#define memory_barrier() asm volatile("" : : : "memory");

asm("modentry:");
asm("mov $stack + 768, %esp");
asm("jmp startc");

static __attribute__((always_inline)) inline uint32_t syscall(uint32_t eax, uint32_t edi, uint32_t esi, uint32_t edx) {
	uint32_t ret;
	asm volatile("int $0xEC" : "=a"(ret) : "a"(eax), "S"(esi), "D"(edi), "d"(edx));
	return ret;
}

static __attribute__((always_inline)) inline void *sys_vpm_map(void *virt, void *phys, size_t length) {
	return (void*) syscall(0, (uint32_t) virt, (uint32_t) phys, length);
}

static __attribute__((always_inline)) inline uint32_t sys_canal_create(uint32_t nameLow, uint32_t nameHigh) {
	return syscall(1, nameLow, nameHigh, 0);
}

static __attribute__((always_inline)) inline void *sys_link_create(uint32_t nameLow, uint32_t nameHigh, size_t areaSize) {
	return (void*) syscall(2, nameLow, nameHigh, areaSize);
}

static __attribute__((always_inline)) inline void *sys_canal_accept(uint32_t canalId) {
	return (void*) syscall(3, canalId, 0, 0);
}

static __attribute__((always_inline)) inline void sys_signal_send(void *area) {
	syscall(4, (uint32_t) area, 0, 0);
}

static __attribute__((always_inline)) inline uint32_t sys_signal_wait() {
	return syscall(5, 0, 0, 0);
}

#endif