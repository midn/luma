HEADERS := $(find src/ -name "*.h")

drive.bin: Makefile linker.ld elftomod.py bootfs/luma_vbe.mod bootfs/luma_wm.mod boot0.bin boot1.bin $(HEADERS) src/kernel/arch/x86/log.o src/kernel/arch/x86/interrupt.o src/kernel/arch/x86/scheduler.o  src/kernel/arch/x86/mem.o src/kernel/arch/x86/ppm.o src/kernel/arch/x86/vpm.o src/kernel/arch/x86/canal.o
	i686-elf-ld -pie -T linker.ld -nodefaultlibs -nostdlib -nostartfiles -o kernel.elf src/kernel/arch/x86/log.o src/kernel/arch/x86/interrupt.o src/kernel/arch/x86/scheduler.o src/kernel/arch/x86/mem.o src/kernel/arch/x86/ppm.o src/kernel/arch/x86/vpm.o src/kernel/arch/x86/canal.o
	./elftomod.py kernel.elf bootfs/krnl.mod ppm_Bitmap:0 vpm_init:1 vpm_map:2 canal_init:3 scheduler_init:4 scheduler_spawn:5 scheduler_start:6
	
	# Create empty drive
	dd if=/dev/zero of=drive.bin bs=1024 count=10240
	# Since fdisk is controlled through user input, we pipe it in
	# I'm not sure where I got the CHS values
	echo -e "o\nn\np\n1\n2048\n\nw\n" | fdisk -u -C20 -S63 -H16 drive.bin
	mkdir -p mnt
	# The -o argument is the offset of the partition we mount
	sudo losetup -o1048576 /dev/loop0 drive.bin
	# Format the partition with ext2
	sudo mkfs.ext2 -r0 -b1024 /dev/loop0
	# Mount the ext2 filesystem into ./mnt
	sudo mount /dev/loop0 ./mnt
	# Copy important files into the mounted fs
	sudo cp -R bootfs/* ./mnt/
	sudo umount ./mnt
	sudo losetup -d /dev/loop0
	
	# Copy first stage of bootloader
	dd if=boot0.bin of=drive.bin bs=1 count=446 seek=0 conv=notrunc
	# Second stage at second sector
	dd if=boot1.bin of=drive.bin bs=1 seek=512 conv=notrunc
	# Fixes BS "last mount path" information leak
	dd if=/dev/zero of=drive.bin bs=1 seek=1049736 count=64 conv=notrunc

bootfs/luma_vbe.mod: Makefile linker.ld elftomod.py luma/vbe/main.c
	i686-elf-gcc -march=i386 -Os -pie -nodefaultlibs -nostdlib -nostartfiles -fomit-frame-pointer -c -o luma/vbe/main.o -ffreestanding -std=gnu99 -Wall -pedantic luma/vbe/main.c
	i686-elf-ld -pie -T linker.ld -nodefaultlibs -nostdlib -nostartfiles -o luma/vbe/main.elf luma/vbe/main.o
	./elftomod.py luma/vbe/main.elf bootfs/luma_vbe.mod

bootfs/luma_wm.mod: Makefile linker.ld elftomod.py luma/wm/main.c
	i686-elf-gcc -march=i386 -Os -pie -nodefaultlibs -nostdlib -nostartfiles -fomit-frame-pointer -c -o luma/wm/main.o -ffreestanding -std=gnu99 -Wall -pedantic luma/wm/main.c
	i686-elf-ld -pie -T linker.ld -nodefaultlibs -nostdlib -nostartfiles -o luma/wm/main.elf luma/wm/main.o
	./elftomod.py luma/wm/main.elf bootfs/luma_wm.mod

boot0.bin: src/boot0.asm
	nasm -fbin -o $@ $<

boot1.bin: src/boot1.asm
	nasm -fbin -o $@ $<

src/kernel/%.o: src/kernel/%.c
	i686-elf-gcc -march=i386 -Isrc -Os -pie -nodefaultlibs -nostdlib -nostartfiles -fomit-frame-pointer -c -o $@ -ffreestanding -std=gnu99 -Wall -pedantic $<

src/kernel/%.o: src/kernel/%.asm
	nasm -felf32 -o $@ $<