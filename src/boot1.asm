cpu 386

bits 16
org 0xE800

%define BIT_READWRITE (1 << 1)
%define BIT_EXECUTABLE (1 << 3)
%define BIT_ISCODEORDATA (1 << 4)
%define BIT_PRESENT (1 << 7)

%define PBIT_PRESENT 1
%define PBIT_WRITABLE 2
%define PBIT_USERLEVEL 4
%define PBIT_WRITETHROUGH 8
%define PBIT_CACHEDISABLE 16
%define PBIT_ACCESSED 32

MALLOC_MEM equ 0x520

start:
	mov ax, 0
	mov es, ax
	mov ss, ax
	mov sp, 0
	
	mov [ld_lba.drive + 1], dl
	
	mov ah, 8
	mov di, 0
	int 0x13
	mov dl, dh
	mov dh, 0
	inc dx
	mov [lba2chs.numheads + 1], dx
	and cl, 0x3F
	mov ch, 0
	mov [lba2chs.spt + 1], cx
	
	mov eax, [0x7C00 + 0x1BE + 8]
	mov [get_inode.abs1], eax
	mov [get_inode.abs2], eax
	mov [read_inode.abs1], eax
	
	mov ecx, 40 * 25
	mov edi, 0
.lup:
	mov [fs:edi + ecx * 4 - 4], dword 0x0F000F00
	dec ecx
	jnz .lup
	
	mov si, MSG_START
	mov ax, MSG_START.end - MSG_START
	call print16
	
	mov edi, MALLOC_MEM ;Pointer to next free location in dalloc'd memory
	
	add edi, 4096
	mov [TSS.esp0], edi ;End of stack ~ beginning of memory map!!
	
ramdetect: ;Currently only detects RAM below 16MB, which will be enough for a while.
	clc
	int 0x12
	jc .fail
	mov [di + 0], dword 0
	mov [di + 4], dword 0
	movzx ecx, ax
	shr ecx, 2
	mov [di + 8], ecx
	mov [di + 12], byte 0
	add di, 13
	mov al, 0
	shr ecx, 3
	rep stosb
	
	xor cx, cx
	xor dx, dx
	mov ax, 0xE801
	int 0x15
	jc .fail
	cmp ah, 0x86
	je .fail
	cmp ah, 0x80
	je .fail
	jcxz .useax
	mov ax, cx
	mov bx, dx
.useax:
	mov [di + 0], dword 0x100000
	mov [di + 4], dword 0
	movzx ecx, ax
	shr ecx, 2
	mov [di + 8], ecx
	mov [di + 12], byte 0
	add di, 13
	mov al, 0
	shr ecx, 3
	rep stosb
	jmp .foundMMap
	
.fail:
	mov si, MSG_FAIL
	mov ax, MSG_FAIL.end - MSG_FAIL
	call print16
	
	jmp $
.foundMMap:
	mov si, MSG_DETECTED_MEMORY_MAP
	mov ax, MSG_DETECTED_MEMORY_MAP.end - MSG_DETECTED_MEMORY_MAP
	call print16
	
pg:
	; Create page directory
	add edi, 4095
	and edi, ~4095
	mov cr3, edi ;In advance
	lea eax, [edi + 4096]
	or eax, PBIT_PRESENT | PBIT_WRITABLE
	mov [edi], eax
	add edi, 4
	mov eax, 0
	mov ecx, 1023
	rep stosd

	mov ecx, 192
	mov edx, PBIT_PRESENT | PBIT_WRITABLE
.lup:
	mov [edi], edx
	add edi, 4
	add edx, 4096
	dec ecx
	jnz .lup
	
	mov ecx, 1024 - 192
	mov eax, 0
	rep stosd
	
	mov eax, cr3 ;Back to the page directory
	mov [eax + 4092], edi
	or dword [eax + 4092], PBIT_PRESENT | PBIT_WRITABLE
	mov [edi + 4092], edi
	or dword [edi + 4092], PBIT_PRESENT | PBIT_WRITABLE
	add edi, 4096
	
	mov esi, MSG_STEP3s
	mov eax, MSG_STEP3s.end - MSG_STEP3s
	call print16
	
initdisk:
	mov eax, [0x7c00 + 0x1BE + 8] ;LBA sector
	add eax, 2 ;The superblock is at sct 2 of volume
	mov bx, di
	call ld_lba
	
	; Only allow block sizes of 1024
	cmp [di + 24], dword 0
	je .blockszcheck
	mov si, MSG_STEP4f
	mov ax, MSG_STEP4f.end - MSG_STEP4f
	call print16
.blockszcheck:
	mov ebx, [di + 40]
	mov [get_inode.inodespergroup + 2], ebx

loadkernel:
	mov eax, 2 ;root
	call get_inode
	mov bx, di
	call read_inode
	mov eax, edi
	mov esi, KERNEL_FILE
	mov ecx, KERNEL_FILE.end - KERNEL_FILE
	call find_in_dirnode
	test eax, eax
	jnz .found
	
	mov si, MSG_STEP5f
	mov ax, MSG_STEP5f.end - MSG_STEP5f
	call print16
	jmp $
.found:
	call get_inode
	mov bx, di
	call read_inode
	; Now edi is kernel location, ebx is after kernel.
	call fix_module_addresses
	mov [inPE.KERNEL_START], edi
	mov edi, ebx ;Allocate modules after kernel
	;~ mov [inPE.MODULES_START], edi
	
loadmods:
	mov eax, 2
	call get_inode
	mov bx, $$ - 0x400 ;Will break if root directory is larger than 1KB.
	call read_inode
	mov eax, $$ - 0x400
.lup: ;Loop over files, the names of which end with ".mod"
	movzx ecx, byte [eax + 6] ;Length of name
	lea edx, [eax + 8] ;Get name string
	cmp ecx, 4 ;Test if shorter than 4 characters
	jb .cont
.testext:
	cmp [eax + ecx + 8 - 4], dword ".mod"
	jne .cont
.testkrnl:
	cmp [eax + 8], dword "krnl"
	je .cont
.ismod:
	push eax
	mov eax, [eax] ;inode
	call get_inode
	add di, 4095
	and di, ~4095
	mov esi, cr3
	mov cx, 1024
	rep movsd ;Copy page directory to one local to process
	mov bx, di
	call read_inode ;edi = start, ebx = end
	mov di, bx
	pop eax
.cont:
	movzx ecx, word [eax + 4]
	add eax, ecx
	cmp eax, $$
	jne .lup
.end:
	mov [inPE.IMPORTANT_MEM_END], edi

fixmmap: ; Make all module memories "used"
	lea ecx, [ebx + 4095]
	shr ecx, 12
	mov ebx, ecx
	and ebx, 7
	shr ecx, 3
	mov edi, [TSS.esp0]
	add edi, 13
	mov al, -1
	rep stosb
	mov ecx, ebx
	mov al, 1
	shl al, cl
	dec al
	mov [edi], al

modex:
	mov ax, 0x13
	int 0x10
	mov dx, 0x3C4
	mov al, 0x04
	out dx, al
	inc dx
	mov al, 6
	out dx, al
	mov dx, 0x3D4
	mov al, 0x14
	out dx, al
	inc dx
	mov al, 0x00
	out dx, al
	dec dx
	mov al, 0x17
	out dx, al
	inc dx
	mov al, 0xE3
	out dx, al
	
	push strict dword inPE
	jmp enterPM

enterPM:
	cli
	or [GDT.R0CSLIMFLAGS], byte 64 ;Make 32-bit
	lgdt [GDT]
	mov eax, cr0
	or eax, 1 | (1 << 31)
	mov cr0, eax
	jmp 8:.inPM
.inPM:
bits 32
	mov eax, 0x23
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov al, 0x10
	mov ss, ax
	ret

enterRM:
	and [GDT.R0CSLIMFLAGS], byte ~64 ;Make 16-bit
	lgdt [GDT]
	jmp 8:.in16
.in16:
bits 16
	mov eax, cr0
	and eax, ~(1 | (1 << 31))
	mov cr0, eax
	jmp 0:.inRM
.inRM:
	mov ax, 0
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax
	sti
	ret

print16:
	push di
	push cx
	push ax
.ind:
		mov di, 0
		mov cx, ax
.lup:
		mov al, [es:si]
		mov [fs:di], al
		inc si
		inc di
		inc di
		dec cx
		jnz .lup
		mov [.ind + 1], di ;SMC
	pop ax
	pop cx
	pop di
	ret

ld_lba:
	pushad
	call lba2chs
	mov ch, al
	mov dh, dl
.drive:
	mov dl, 0
	mov ah, 2
	mov al, 1
	int 0x13
	popad
	ret

; In:	AX = lba
; Out:	CX = sector, AX = cylinder, DX = head
lba2chs:
	push bx
.spt:
	mov bx, 0xF157
	xor dx, dx
	div bx
	mov cx, dx
	inc cx
.numheads:
	mov bx, 0x9001
	xor dx, dx
	div bx
	pop bx
	ret

; In:	EAX = inode
; Out:	EDX = inode struct pointer
get_inode:
	push ebx
	push ecx
	
	dec eax
.inodespergroup:
	mov ebx, 0x12345678
	xor edx, edx
	div ebx ;eax = blockgroup, edx = index in group
	
	; Find descriptor
	mov ecx, eax
	and ecx, 15
	shl ecx, 5 ;Byte offset into sector
	shr eax, 4 ;Sector relative to blockgroup descriptors
.abs1 equ ($ + 2)
	add eax, strict dword 0x12345678
	add eax, 4 ;Absolute sector
	mov bx, $$ - 0x800
	call ld_lba
	mov eax, [$$ - 0x800 + ecx + 8] ;block /w inode table
	shl eax, 1 ;block to sector
.abs2 equ ($ + 2)
	add eax, strict dword 0
	mov ecx, edx
	and ecx, 3
	shl ecx, 7
	shr edx, 2
	add eax, edx
	call ld_lba
	
	lea edx, [$$ - 0x800 + ecx]
	
	pop ecx
	pop ebx
	ret

; In:	EDX = inode struct pointer
; Out:	*ES:BX = data
read_inode:
.abs1 equ ($ + 2)
	mov ecx, strict dword 0
	
	mov eax, [edx + 40] ;Direct block 0
	shl eax, 1 ;block to sector
	add eax, ecx
	call ld_lba
	inc eax
	add bx, 512
	call ld_lba
	add bx, 512
	
	mov eax, [edx + 44] ;Direct block 1
	test eax, eax
	jz .quit
	shl eax, 1 ;block to sector
	add eax, ecx
	call ld_lba
	inc eax
	add bx, 512
	call ld_lba
	add bx, 512
	
	mov eax, [edx + 48] ;Direct block 2
	test eax, eax
	jz .quit
	shl eax, 1 ;block to sector
	add eax, ecx
	call ld_lba
	inc eax
	add bx, 512
	call ld_lba
	add bx, 512
	
	mov eax, [edx + 52] ;Direct block 3
	test eax, eax
	jz .quit
	shl eax, 1 ;block to sector
	add eax, ecx
	call ld_lba
	inc eax
	add bx, 512
	call ld_lba
	add bx, 512
	
	mov eax, [edx + 56] ;Direct block 4
	test eax, eax
	jz .quit
	shl eax, 1 ;block to sector
	add eax, ecx
	call ld_lba
	inc eax
	add bx, 512
	call ld_lba
	add bx, 512
	
	mov eax, [edx + 60] ;Direct block 5
	test eax, eax
	jz .quit
	shl eax, 1 ;block to sector
	add eax, ecx
	call ld_lba
	inc eax
	add bx, 512
	call ld_lba
	add bx, 512
	
	mov eax, [edx + 64] ;Direct block 6
	test eax, eax
	jz .quit
	shl eax, 1 ;block to sector
	add eax, ecx
	call ld_lba
	inc eax
	add bx, 512
	call ld_lba
	add bx, 512
	
	mov eax, [edx + 68] ;Direct block 7
	test eax, eax
	jz .quit
	shl eax, 1 ;block to sector
	add eax, ecx
	call ld_lba
	inc eax
	add bx, 512
	call ld_lba
	add bx, 512
	
.quit:
	ret

; In:	*EAX = directory content buffer, *ESI = name to test, ECX = name length
; Out:	EAX = inode or 0 if not found
find_in_dirnode:
	push edx
	push edi
.lup:
	cmp [eax + 0], dword 0
	je .end
	
	movzx edx, byte [eax + 6] ; ecx: Size of name
	cmp edx, ecx
	jne .next
	
	lea edi, [eax + 8]
	call strncmp
	je .end
.next:
	lea eax, [eax + edx + 8]
	add eax, 3
	and eax, ~3
	jmp .lup
.end:
	mov eax, [eax + 0]
	pop edi
	pop edx
	ret

; In:	*ESI = string buffer 1, *EDI = string buffer 2, ECX = max size check
; Out:	equal <-> ZF
strncmp:
	push eax
	push esi
	push edi
	push ecx
.lup:
	mov al, [esi]
	cmp al, [edi]
	jne .r
	inc esi
	inc edi
	dec ecx
	jnz .lup
.r:
	pop ecx
	pop edi
	pop esi
	pop eax
	ret

fix_module_addresses:
	push esi
	push ecx
	push eax
	push ebx
	
	movzx ecx, word [edi + 4] ;amount of syms
	lea esi, [edi + 12 + ecx * 4]
	lea esi, [esi + ecx * 2]
	movzx ecx, word [edi + 6] ;amount of relocs
	mov eax, esi ;ptr to relocs
	lea esi, [esi + ecx * 4] ;ptr to data
	mov [edi], esi ;Replace the MOD\0 magic header with the base address
	test ecx, ecx ;If none, end
	jz .end
.lup:
	; Apply relocation
	mov ebx, [eax]
	add ebx, esi
	add [ebx], esi
	
	add eax, 4
	dec ecx
	jnz .lup
.end:
	
	pop ebx
	pop eax
	pop ecx
	pop esi
	ret

bits 32

inPE:
	mov [TSS.ss0], dword 0x10
	mov esp, [TSS.esp0]
	
	mov eax, 40
	ltr ax
	
	mov ax, [print16.ind + 1]
	mov [print32.LAST], ax
	
	mov esi, MSG_STEP2s
	mov eax, MSG_STEP2s.end - MSG_STEP2s
	call print32
	
.KERNEL_START equ ($ + 1)
	mov edi, strict dword 0
	lea ebx, [edi + 14]
	; Sym 0: ppm_Bitmap
	mov eax, [ebx]
	add eax, [edi] ;Base address that used to be magic header
	push dword [TSS.esp0] ;As said before, end of stack ~ beginning of mmap!
	pop dword [eax]
	add ebx, 6
	; Sym 1: vpm_init
	mov eax, [ebx]
	add eax, [edi]
.IMPORTANT_MEM_END equ ($ + 1)
	push strict dword 0
	call eax
	add esp, 4
	add ebx, 6
	; Sym 2: vpm_map
	mov eax, [ebx]
	add eax, [edi]
	mov [.VPM_MAP_ENTRY], eax
	add ebx, 6
	; Sym 3: canal_init
	mov eax, [ebx]
	add eax, [edi]
	call eax
	add ebx, 6
	; Sym 4: scheduler_init
	mov eax, [ebx]
	add eax, [edi]
	call eax
	add ebx, 6
	; Sym 5: scheduler_spawn
	mov eax, [ebx]
	add eax, [edi]
	mov [.SCHEDULER_SPAWN_ENTRY], eax
	add ebx, 6
	; Sym 6: scheduler_start
	mov eax, [ebx]
	add eax, [edi]
	mov [.SCHEDULER_START_ENTRY], eax
	
	mov ebp, 2
.modspawnlup:
	; Find next module
	movzx ecx, word [edi + 4]
	lea eax, [edi + ecx * 4]
	lea eax, [eax + ecx * 2]
	movzx ecx, word [edi + 6]
	lea eax, [eax + ecx * 4]
	mov ecx, [edi + 8]
	lea eax, [eax + ecx]
	add eax, 4096 + 4095
	and eax, ~4095
	mov edi, eax
	mov ebx, 0x40000000
	call rebase_module32
	sub edx, edi
	
	push strict dword edx
	push strict dword edi
	push strict dword 0x40000000
	push strict dword 1 ;user
	lea eax, [edi - 4096] ; cr3
	push eax
.VPM_MAP_ENTRY equ ($ + 1)
	mov eax, strict dword 0
	call eax
	add esp, 20
	
.SCHEDULER_SPAWN_ENTRY equ ($ + 1)
	mov eax, strict dword 0
	call eax
	lea ebx, [edi - 4096]
	mov [eax + 46], ebx ;cr3
	mov ebx, [edi]
	mov [eax + 34], ebx ;eip
	
	dec ebp
	jnz .modspawnlup
	
.SCHEDULER_START_ENTRY equ ($ + 1)
	mov eax, strict dword 0
	jmp eax

print32:
	push ebx
	push eax
	push ecx
	movzx ebx, word [.LAST]
	mov ecx, eax
.lup:
	mov al, [esi]
	mov [0xB8000 + ebx], al
	inc esi
	inc ebx
	inc ebx
	dec ecx
	jnz .lup
	mov [.LAST], bx
	pop ecx
	pop eax
	pop ebx
	ret
.LAST: dw 0

; In: EDI = module pointer, EBX = rebase address
; Out: EDX = physical module end
rebase_module32:
	push ecx
	push eax
	push ebx
	push ebp
	movzx ecx, word [edi + 4]
	lea eax, [edi + 12 + ecx * 2]
	lea eax, [eax + ecx * 4] ; eax = relocations pointer
	movzx ecx, word [edi + 6] ;ecx = amount of relocations
	lea edx, [eax + ecx * 4] ; edx = data pointer
	mov [edi], edx ;Store offset of data buffer in
	sub [edi], edi ;file in place of magic header
	add [edi], ebx ;
	mov ebx, [edi]
	test ecx, ecx
	jz .end
.lup:
	mov ebp, [eax]
	add [edx + ebp], ebx
	add eax, 4
	dec ecx
	jnz .lup
.end:
	add edx, [edi + 8]
	pop ebp
	pop ebx
	pop eax
	pop ecx
	ret

load_module32:
	

MSG_FAIL: db "Failed to detect memory map. "
.end:

MSG_START: db "Starting eklernel. "
.end:

MSG_DETECTED_MEMORY_MAP: db "Found memory map. "
.end:

MSG_STEP2s: db "Entered protected mode. "
.end:

MSG_STEP3s: db "Generated paging structures. "
.end:

MSG_STEP4f: db "Boot filesystem must have block size of 1024. "
.end:

KERNEL_FILE: db "krnl.mod"
.end:

MSG_STEP5f: db "Kernel not found; aborting boot. "
.end:

GDT:
	dw (GDT.END - GDT) - 1 ;GDT descriptor in the null entry
	dd GDT
	dw 0
	
	dw 0xFFFF, 0x0000
	db 0x00, BIT_READWRITE | BIT_EXECUTABLE | BIT_ISCODEORDATA | BIT_PRESENT | (0 << 5)
.R0CSLIMFLAGS:
	db (128 | 64) | 0xF
	db 0
	
	dw 0xFFFF, 0x0000
	db 0x00, BIT_READWRITE | BIT_ISCODEORDATA | BIT_PRESENT | (0 << 5)
	db (128 | 64) | 0xF
	db 0
	
	dw 0xFFFF, 0x0000
	db 0x00, BIT_READWRITE | BIT_EXECUTABLE | BIT_ISCODEORDATA | BIT_PRESENT | (3 << 5)
	db (128 | 64) | 0xF
	db 0
	
	dw 0xFFFF, 0x0000
	db 0x00, BIT_READWRITE | BIT_ISCODEORDATA | BIT_PRESENT | (3 << 5)
	db (128 | 64) | 0xF
	db 0
	
	dw (TSS.END - TSS) - 1
	dw TSS
	db 0, 1 | 8 | 128
	db 0, 0
.END:
TSS:
	dd 0
.esp0: dd 0
.ss0: dd 0
	times 23 dd 0
.END:

end:
times (end - start + 511) / 512 * 512 - ($ - $$) db 0