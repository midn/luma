#ifndef _SCHEDULER_H
#define _SCHEDULER_H

#include"process.h"
#include<stddef.h>

void scheduler_init();
void scheduler_switch();
Process *scheduler_spawn();

extern size_t scheduler_ProcessCount;
extern Process *scheduler_ProcessList;

#endif