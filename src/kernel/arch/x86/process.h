#ifndef _PROCESS_H
#define _PROCESS_H

#include<stdint.h>

typedef struct __attribute__((packed)) {
	uint32_t eax;    //0
	uint32_t ebx;    //4
	uint32_t ecx;    //8
	uint32_t edx;    //12
	uint32_t esi;    //16
	uint32_t edi;    //20
	uint32_t ebp;    //24
	uint32_t esp;    //28
	uint32_t eip;    //32
	uint32_t eflags; //36
	uint32_t cs;     //40
	uint32_t cr3;    //44
} Context;

typedef struct __attribute__((packed)) {
	uint16_t id;
	Context ctx;
	uint8_t waiting;
} Process;

#endif