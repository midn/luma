#include"ppm.h"

#include<stdint.h>

typedef struct {
	uint64_t start;
	uint32_t length;
	uint8_t flags;
	uint8_t data[];
} PPMBitmap;

PPMBitmap *ppm_Bitmap;

uint64_t ppm_alloc() {
	for(uint32_t i = 0; i < ppm_Bitmap->length / 8; i++) {
		for(int b = 0; b < 8; b++) {
			if((ppm_Bitmap->data[i] & (1 << b)) == 0) {
				ppm_Bitmap->data[i] = ppm_Bitmap->data[i] | (1 << b);
				return ppm_Bitmap->start + (i * 8 + b) * 4096;
			}
		}
	}
	return 0;
}