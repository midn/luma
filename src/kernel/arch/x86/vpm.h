#ifndef _VPM_H
#define _VPM_H

#include<stdint.h>
#include<stddef.h>

typedef struct {
	uint32_t from, to;
} SharedRange;

typedef enum {
	VPM_KERNEL, VPM_USER
} AllocationOwner;

void set_temporary_mapping(uint32_t addr);

uint32_t vpm_find_free(uint32_t addrspace, AllocationOwner owner, size_t len);
void *vpm_map(uint32_t cr3, AllocationOwner owner, uint32_t virt, uint32_t phys, size_t len);
uint32_t vpm_create_space();
void vpm_set_space(uint32_t cr3);
uint32_t vpm_get_phys(uint32_t cr3, uint32_t);

#endif