#include"mem.h"

void kmemw1(void *_ptr, uint8_t val, size_t amount) {
	uint8_t *ptr = _ptr;
	for(size_t i = 0; i < amount; i++) {
		*(ptr++) = val;
	}
}

void kmemw4(void *_ptr, uint32_t val, size_t amount) {
	uint32_t *ptr = _ptr;
	for(size_t i = 0; i < amount; i++) {
		*(ptr++) = val;
	}
}