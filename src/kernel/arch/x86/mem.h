#ifndef _MEM_H
#define _MEM_H

#include<stdint.h>
#include<stddef.h>

void kmemw1(void *ptr, uint8_t val, size_t amount);
void kmemw4(void *ptr, uint32_t val, size_t amount);

#endif