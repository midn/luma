#include"canal.h"

#include"vpm.h"
#include"mem.h"
#include"scheduler.h"
#include"ppm.h"

static Canal *canals = 0;
static Link *links = 0;

static int lastSignalId = 0;
static uint32_t *signals = 0;

void canal_init() {
	uint32_t cr3;
	asm volatile("movl %%cr3, %0" : "=r"(cr3) :);
	
	canals = vpm_map(cr3, VPM_KERNEL, -1, -1, 4096);
	kmemw1(canals, 0, 4096);
	
	links = vpm_map(cr3, VPM_KERNEL, -1, -1, 4096);
	kmemw1(links, 0xFF, 4096);
	
	signals = vpm_map(cr3, VPM_KERNEL, -1, -1, 4096);
	kmemw4(signals, 0x3FFFFFFF, 1024);
}

uint32_t canal_create(uint64_t name, uint16_t hostProcessId) {
	for(int i = 0; i < 4096 / sizeof(Canal); i++) {
		if(canals[i].name == 0) {
			canals[i].name = name;
			canals[i].hostProcessId = hostProcessId;
			return i;
		}
	}
	return -1;
}

uint32_t link_create(uint64_t canalName, uint16_t guestProcessId, uint32_t areaSize) {
	int canalId = -1;
	for(int i = 0; i < 4096 / sizeof(Canal); i++) {
		if(canals[i].name == canalName) {
			canalId = i;
			break;
		}
	}
	
	if(canalId == -1) return -1;
	if(canals[canalId].hostProcessId == guestProcessId) return -1;
	
	uint32_t hostCR3 = scheduler_ProcessList[canals[canalId].hostProcessId].ctx.cr3;
	uint32_t hostAreaVirtAddr = vpm_find_free(hostCR3, VPM_USER, areaSize);
	
	uint32_t guestCR3 = scheduler_ProcessList[guestProcessId].ctx.cr3;
	uint32_t guestAreaVirtAddr = vpm_find_free(guestCR3, VPM_USER, areaSize);
	
	/* TODO: Fail if areas not found. */
	
	for(int i = 0; i < 4096 / sizeof(Link); i++) {
		if(links[i].canalId == -1) {
			links[i].canalId = canalId;
			links[i].guestProcessId = guestProcessId;
			links[i].hostVirtAddr = hostAreaVirtAddr;
			links[i].guestVirtAddr = guestAreaVirtAddr;
			links[i].flags = 0;
			
			uint32_t pages = (areaSize + 4095) / 4096;
			for(int p = 0; p < pages; p++) {
				uint32_t phys = ppm_alloc();
				vpm_map(hostCR3, VPM_USER, hostAreaVirtAddr + p * 4096, phys, 4096);
				vpm_map(guestCR3, VPM_USER, guestAreaVirtAddr + p * 4096, phys, 4096);
			}
			
			return guestAreaVirtAddr;
		}
	}
	
	return -1;
}

uint32_t canal_accept(uint16_t hostProcessId, uint32_t canalId) {
	if(canals[canalId].hostProcessId != hostProcessId) {
		return -1;
	}
	
	for(int i = 0; i < 4096 / sizeof(Link); i++) {
		if(links[i].canalId == canalId && !(links[i].flags & LINK_FLAG_ACCEPTED)) {
			links[i].flags = links[i].flags | LINK_FLAG_ACCEPTED;
			return links[i].hostVirtAddr;
		}
	}
	
	return -1;
}

void link_send_signal_from(uint16_t processId, uint32_t processAreaAddr) {
	for(int i = 0; i < 4096 / sizeof(Link); i++) {
		if(links[i].guestProcessId == processId && links[i].guestVirtAddr == processAreaAddr) {
			signals[lastSignalId] = i | SIGNAL_FLAG_G2H;
			lastSignalId++;
			return;
		} else if(canals[links[i].canalId].hostProcessId == processId && links[i].hostVirtAddr == processAreaAddr) {
			signals[lastSignalId] = i | SIGNAL_FLAG_H2G;
			lastSignalId++;
			return;
		}
	}
}

static void remove_signal(int s) {
	for(int i = s + 1; i < 1024; i++) {
		signals[i - 1] = signals[i];
	}
	signals[1023] = 0x3FFFFFFF;
	lastSignalId--;
}

uint32_t link_get_oldest_signal_to(uint16_t processId) {
	for(int s = 0; s < 1024; s++) {
		int l = signals[s] & 0x3FFFFFFF;
		int f = signals[s] & ~0x3FFFFFFF;
		
		if(l == 0x3FFFFFFF) goto notfound;
		
		if(links[l].guestProcessId == processId && (f & SIGNAL_FLAG_H2G)) {
			remove_signal(s);
			return links[l].guestVirtAddr;
		} else if(canals[links[l].canalId].hostProcessId == processId && (f & SIGNAL_FLAG_G2H)) {
			remove_signal(s);
			return links[l].hostVirtAddr;
		}
	}
notfound:
	return -1;
}