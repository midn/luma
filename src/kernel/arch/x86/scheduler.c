#include"scheduler.h"

#include"vpm.h"
#include"mem.h"

#include<kernel/arch/x86/consts.h>

size_t scheduler_ProcessCount = 0;
Process *scheduler_ProcessList = 0;

volatile Process *scheduler_CurrentProcess = 0;

void scheduler_init() {
	uint32_t cr3;
	asm volatile("movl %%cr3, %0" : "=r" (cr3) :);
	
	scheduler_ProcessList = vpm_map(cr3, VPM_KERNEL, -1, -1, 4096);
	
	scheduler_ProcessCount = 0;
}

static int consider_switch(int i) {
	if(scheduler_ProcessList[i].waiting) {
		uint32_t lastSignal = link_get_oldest_signal_to(scheduler_ProcessList[i].id);
		if(lastSignal != -1) {
			scheduler_ProcessList[i].waiting = 0;
			scheduler_ProcessList[i].ctx.eax = lastSignal;
		} else return 0;
	}
	
	scheduler_CurrentProcess = &scheduler_ProcessList[i];
	return 1;
}

void scheduler_switch() {
	static size_t i = 0;
	if(scheduler_ProcessCount == 0) {
		scheduler_CurrentProcess = 0;
		i = 0;
	} else {
		for(int j = i; j < scheduler_ProcessCount; j++) {
			if(consider_switch(j)) {
				i = (j + 1) % scheduler_ProcessCount;
				return;
			}
		}
		for(int j = 0; j < i; j++) {
			if(consider_switch(j)) {
				i = (j + 1) % scheduler_ProcessCount;
				return;
			}
		}
		scheduler_CurrentProcess = 0;
	}
}

Process *scheduler_spawn() {
	volatile Process *ret = &scheduler_ProcessList[scheduler_ProcessCount];
	kmemw1(ret, 0, sizeof(*ret));
	ret->id = scheduler_ProcessCount++;
	ret->ctx.eflags = EFLAGS_RESET | EFLAGS_IF | EFLAGS_IOPL_3;
	ret->ctx.cs = 24 | RPL_3;
	
	return ret;
}