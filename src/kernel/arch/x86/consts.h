#ifndef _X86_CONSTS_H
#define _X86_CONSTS_H

#define EFLAGS_IOPL_0 (0 << 12)
#define EFLAGS_IOPL_1 (1 << 12)
#define EFLAGS_IOPL_2 (2 << 12)
#define EFLAGS_IOPL_3 (3 << 12)
#define EFLAGS_IF (1 << 9)
#define EFLAGS_RESET 2

#define RPL_0 0
#define RPL_1 1
#define RPL_2 2
#define RPL_3 3

#define PAGE_BIT_PRESENT 1
#define PAGE_BIT_WRITABLE 2
#define PAGE_BIT_USER 4

#endif