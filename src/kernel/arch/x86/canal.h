#ifndef _CANAL_H
#define _CANAL_H

#include<stdint.h>

#define LINK_FLAG_ACCEPTED 1

#define SIGNAL_FLAG_G2H 0x80000000
#define SIGNAL_FLAG_H2G 0x40000000

typedef struct {
	uint64_t name;
	uint16_t hostProcessId;
} __attribute__((packed)) Canal;

typedef struct {
	uint32_t canalId;
	uint32_t hostVirtAddr;
	uint16_t guestProcessId;
	uint32_t guestVirtAddr;
	uint8_t flags;
} __attribute__((packed)) Link;

uint32_t canal_create(uint64_t name, uint16_t hostProcessId);
uint32_t link_create(uint64_t canal, uint16_t guestProcessId, uint32_t areaSize);
uint32_t canal_accept(uint16_t hostProcessId, uint32_t canalId);
void link_send_signal_from(uint16_t processId, uint32_t processAreaAddr);
uint32_t link_get_oldest_signal_to(uint16_t processId);

#endif