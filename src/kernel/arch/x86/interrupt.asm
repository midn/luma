cpu 386
bits 32

; TODO: perform IO waits

section .text

extern scheduler_switch
extern scheduler_CurrentProcess
pit_handler:
	push eax
	
	mov al, 0x20
	out 0x20, al
	
	mov eax, [scheduler_CurrentProcess]
	test eax, eax
	jnz .a
	add esp, 16
	jmp .b
.a:
	pop dword [eax + 2] ;eax
	pop dword [eax + 34] ;eip
	pop dword [eax + 42] ;cs
	pop dword [eax + 38] ;eflags
	mov [eax + 6], ebx
	mov [eax + 10], ecx
	mov [eax + 14], edx
	mov [eax + 18], esi
	mov [eax + 22], edi
	mov [eax + 26], ebp
	cmp dword [eax + 42], 8 ;CS 8 implies ring 0
	jne .isR3
	mov [eax + 30], esp
	jmp .b
.isR3:
	pop dword [eax + 30] ;esp
	add esp, 4 ;ss
.b:
	call scheduler_switch
	jmp jump_to_process

global jump_to_process
jump_to_process:
	mov eax, [scheduler_CurrentProcess]
	test eax, eax
	jnz .c
	; No next process, goto hlt loop.
	push dword 0x202
	push dword 8
	push haltLoop
	iretd
.c:
	mov ebx, [eax + 46]
	mov cr3, ebx
	mov ebx, [eax + 6]
	mov ecx, [eax + 10]
	mov edx, [eax + 14]
	mov esi, [eax + 18]
	mov edi, [eax + 22]
	mov ebp, [eax + 26]
	cmp dword [eax + 42], 8
	je .isR0
	push dword 32 | 3
	push dword [eax + 30]
	jmp .b
.isR0:
	mov esp, [eax + 30]
.b:
	push dword [eax + 38] ;eflags
	push dword [eax + 42] ;cs
	push dword [eax + 34] ;eip
	mov eax, [eax + 2]
	iretd

pgf_handler:
	add esp, 4
	mov [0xB8000], dword "P_F_"
.LOUP:
	hlt
	jmp .LOUP

dbf_handler:
	add esp, 4
	mov [0xB8000], dword "D_F_"
.LOUP:
	hlt
	jmp .LOUP

gpf_handler:
	pop eax
	movzx ebx, al
	and bl, 0xF
	mov bl, [.tbl + ebx]
	mov [0xB800E], bl
	movzx ebx, al
	shr bl, 4
	mov bl, [.tbl + ebx]
	mov [0xB800C], bl
	shr eax, 8
	movzx ebx, al
	and bl, 0xF
	mov bl, [.tbl + ebx]
	mov [0xB800A], bl
	movzx ebx, al
	shr bl, 4
	mov bl, [.tbl + ebx]
	mov [0xB8008], bl
	shr eax, 8
	movzx ebx, al
	and bl, 0xF
	mov bl, [.tbl + ebx]
	mov [0xB8006], bl
	movzx ebx, al
	shr bl, 4
	mov bl, [.tbl + ebx]
	mov [0xB8004], bl
	shr eax, 8
	movzx ebx, al
	and bl, 0xF
	mov bl, [.tbl + ebx]
	mov [0xB8002], bl
	movzx ebx, al
	shr bl, 4
	mov bl, [.tbl + ebx]
	mov [0xB8000], bl
	shr eax, 8
.LOUP:
	hlt
	jmp .LOUP
.tbl: db "0123456789ABCDEF"

spur_handler:
	iret

syscall_int_entry:
	cmp eax, (.tblend - .tbl) / 4
	jae .invalid
	jmp [.tbl + eax * 4]
.invalid:
	mov eax, 0
	iret
.tbl:
	dd .sys_vpm_map, .sys_canal_create, .sys_link_create, .sys_canal_accept, .sys_signal_send, .sys_signal_wait
.tblend:
.sys_vpm_map:
	push ecx
	push edx
	push esi
	push edi
	push 1
	mov eax, cr3
	push eax
extern vpm_map
	call vpm_map
	add esp, 16
	pop edx
	pop ecx
	iret
.sys_canal_create:
	push ecx
	push edx
	mov eax, [scheduler_CurrentProcess]
	movzx eax, word [eax] ;process ID
	push eax
	push esi
	push edi
extern canal_create
	call canal_create
	add esp, 12
	pop edx
	pop ecx
	iret
.sys_link_create:
	push ecx
	push edx
	mov eax, [scheduler_CurrentProcess]
	movzx eax, word [eax]
	push eax
	push esi
	push edi
extern link_create
	call link_create
	add esp, 12
	pop edx
	pop ecx
	iret
.sys_canal_accept:
	push ecx
	push edx
	push edi
	mov eax, [scheduler_CurrentProcess]
	movzx eax, word [eax] ;id
	push eax
extern canal_accept
	call canal_accept
	add esp, 8
	pop edx
	pop ecx
	iret
.sys_signal_send:
	push ecx
	push edx
	push edi
	mov eax, [scheduler_CurrentProcess]
	movzx eax, word [eax] ;id
	push eax
extern link_send_signal_from
	call link_send_signal_from
	pop eax
	add esp, 4
	pop edx
	pop ecx
	iret
.sys_signal_wait:
	push eax
	mov eax, [scheduler_CurrentProcess]
	mov [eax + 50], byte 1
	pop eax
	jmp pit_handler

global scheduler_start:
scheduler_start:
.pic_init:
	mov al, 0x10 | 0x01
	out 0x20, al
	out 0xA0, al
	
	mov al, 32
	out 0x21, al
	mov al, 40
	out 0xA1, al
	
	mov al, 4
	out 0x21, al
	mov al, 2
	out 0xA1, al
	
	mov al, 0x01
	out 0x21, al
	out 0xA1, al
	 
.pic_mask:
	mov al, ~1 ;PIT
	out 0x21, al
	mov al, ~0
	out 0xA1, al
	
.idt:
	; Page fault handler
	mov eax, pgf_handler
	and eax, 0x0000FFFF
	or eax, 0x00080000
	mov [idt + 14 * 8], eax
	
	mov eax, pgf_handler
	and eax, 0xFFFF0000
	or eax, (14 << 8) | (1 << 15)
	mov [idt + 14 * 8 + 4], eax
	
	; Double fault handler
	mov eax, dbf_handler
	and eax, 0x0000FFFF
	or eax, 0x00080000
	mov [idt + 8 * 8], eax
	
	mov eax, dbf_handler
	and eax, 0xFFFF0000
	or eax, (14 << 8) | (1 << 15)
	mov [idt + 8 * 8 + 4], eax
	
	; General protection fault handler
	mov eax, gpf_handler
	and eax, 0x0000FFFF
	or eax, 0x00080000
	mov [idt + 13 * 8], eax
	
	mov eax, gpf_handler
	and eax, 0xFFFF0000
	or eax, (14 << 8) | (1 << 15)
	mov [idt + 13 * 8 + 4], eax
	
	; Timer scheduler handler
	mov eax, pit_handler
	and eax, 0x0000FFFF
	or eax, 0x00080000
	mov [idt + 32 * 8], eax
	
	mov eax, pit_handler
	and eax, 0xFFFF0000
	or eax, (14 << 8) | (1 << 15)
	mov [idt + 32 * 8 + 4], eax
	
	; Spurious interrupt handler
	mov eax, spur_handler
	and eax, 0x0000FFFF
	or eax, 0x00080000
	mov [idt + 39 * 8], eax
	
	mov eax, spur_handler
	and eax, 0xFFFF0000
	or eax, (14 << 8) | (1 << 15)
	mov [idt + 39 * 8 + 4], eax
	
	; Software interrupt syscall entry point
	mov eax, syscall_int_entry
	and eax, 0x0000FFFF
	or eax, 0x00080000
	mov [idt + 0xEC * 8], eax
	
	mov eax, syscall_int_entry
	and eax, 0xFFFF0000
	or eax, (14 << 8) | (1 << 15) | (3 << 13)
	mov [idt + 0xEC * 8 + 4], eax
	
	push idt
	push (idt.end - idt - 1) << 16
	lidt [esp + 2]
	add esp, 8
	
.pit:
	mov al, (2 << 1) | (2 << 4)
	out 0x43, al
	mov al, 155
	out 0x40, al
	mov al, 46
	out 0x40, al
	
	sti
	
haltLoop:
	hlt
	jmp haltLoop
	
idt:
	times 256 dq 0
.end: