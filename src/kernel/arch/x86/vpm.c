#include"vpm.h"

#include"ppm.h"
#include"mem.h"

extern void *_kernel_end;

void set_temporary_mapping(uint32_t addr) {
	if((addr & 0xFFF) != 0) asm volatile("xchgw %bx, %bx");
	
	*(volatile uint32_t*) 0xFFFFFFF8 = addr | 3;
	asm volatile("invlpg 0xFFFFE000");
}

static uint32_t nextAllocs[2] = {[VPM_KERNEL] = 0, [VPM_USER] = 1 * 1024 * 1024 * 1024};

void vpm_init(uint32_t kernelEnd) {
	nextAllocs[VPM_KERNEL] = (kernelEnd + 4095) & ~4095;
}

/* TODO: Make better. */
uint32_t vpm_find_free(uint32_t addrspace, AllocationOwner owner, size_t len) {
	len = (len + 4095) / 4096;
	
	uint32_t ret = nextAllocs[owner];
	for(size_t i = 0; i < len;) {
		uint32_t pdeid = (ret + i * 4096) >> 22;
		uint32_t pteid = ((ret + i * 4096) >> 12) & 1023;
		
		set_temporary_mapping(addrspace);
		
		volatile uint32_t *pde = &((volatile uint32_t*) 0xFFFFE000)[pdeid];
		if((*pde & 1) == 0) {
			i += 4096;
		} else {
			set_temporary_mapping(*pde & ~0xFFF);
			
			if((((volatile uint32_t*) 0xFFFFE000)[pteid] & 1) == 0) {
				i++;
			} else {
				ret += (i + 1) * 4096;
				i = 0;
			}
		}
	}
	
	return ret;
}

/* Will overwrite! */
/* If phys is -1, map to uncontiguous physical pages anywhere, else map contiguous span of physical memory. */
/* If virt is -1, find free space, else overwrite. */
void *vpm_map(uint32_t addrspace, AllocationOwner owner, uint32_t virt, uint32_t phys, size_t len) {
	uint32_t thisspace;
	asm volatile("movl %%cr3, %0" : "=r" (thisspace) :);
	
	len = (len + 4095) / 4096;
	
	if(virt == -1) virt = vpm_find_free(addrspace, owner, len);
	
	for(size_t i = 0; i < len; i++) {
		set_temporary_mapping(addrspace);
		
		uint32_t pdeid = (virt + i * 4096) >> 22;
		uint32_t pteid = ((virt + i * 4096) >> 12) & 1023;
		
		volatile uint32_t *pde = &((volatile uint32_t*) 0xFFFFE000)[pdeid];
		
		if((*pde & 1) == 0) {
			*pde = ppm_alloc() | 3 | (owner == VPM_USER ? 4 : 0);
		}
		set_temporary_mapping(*pde & ~0xFFF);
		
		volatile uint32_t *pte = &((volatile uint32_t*) 0xFFFFE000)[pteid];
		*pte = (phys == -1 ? ppm_alloc() : (phys + i * 4096)) | 3 | (owner == VPM_USER ? 4 : 0);
		
		if(thisspace == addrspace) {
			//~ asm volatile("xchg %bx, %bx");
			asm volatile("invlpg %0" : : "m"(*(char*) (virt + i * 4096)));
		}
	}
	
	return (void*) virt;
}

uint32_t vpm_create_space() {
	uint32_t curPD;
	asm volatile("movl %%cr3, %0" : "=r"(curPD) :);
	set_temporary_mapping(curPD);
	
	uint32_t pde0000 = *(volatile uint32_t*) 0xFFFFE000;
	uint32_t pde1023 = *(volatile uint32_t*) 0xFFFFEFFC;
	
	uint32_t newPD = ppm_alloc();
	set_temporary_mapping(newPD);
	
	*(volatile uint32_t*) 0xFFFFE000 = pde0000;
	kmemw4((void*) 0xFFFFE004, 0, 1022);
	*(volatile uint32_t*) 0xFFFFEFFC = pde1023;
	
	return newPD;
}

void vpm_set_space(uint32_t cr3) {
	asm volatile("movl %0, %%cr3" : : "r"(cr3));
}

uint32_t vpm_get_phys(uint32_t cr3, uint32_t v) {
	uint32_t pdeid = v >> 22;
	uint32_t pteid = (v >> 12) & 1023;
	
	set_temporary_mapping(cr3);
	set_temporary_mapping(((uint32_t*) 0xFFFFE000)[pdeid] & ~0xFFF);
	
	return ((uint32_t*) 0xFFFFE000)[pteid] & ~0xFFF;
}