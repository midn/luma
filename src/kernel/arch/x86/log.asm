section .data

section .text

global klogc
klogc:
	push eax
	mov eax, ecx
	out 0xE9, al
	pop eax
	ret

global klogs
klogs:
	push esi
	mov esi, ecx
.l:
	mov cl, [esi]
	inc esi
	test cl, cl
	jz .r
	call klogc
	jmp .l
.r:
	pop esi
	ret

;~ global klogp
;~ klogp:
	;~ push eax
	;~ mov edx, [esp + 12]
	;~ mov eax, edx
	;~ shr eax, 28
	;~ call .logh
	;~ mov eax, edx
	;~ shr eax, 24
	;~ and al, 0x0F
	;~ call .logh
	;~ mov eax, edx
	;~ shr eax, 20
	;~ and al, 0x0F
	;~ call .logh
	;~ mov eax, edx
	;~ shr eax, 16
	;~ and al, 0x0F
	;~ call .logh
	;~ mov eax, edx
	;~ shr eax, 12
	;~ and al, 0x0F
	;~ call .logh
	;~ mov eax, edx
	;~ shr eax, 8
	;~ and al, 0x0F
	;~ call .logh
	;~ mov eax, edx
	;~ shr eax, 4
	;~ and al, 0x0F
	;~ call .logh
	;~ mov eax, edx
	;~ and al, 0x0F
	;~ call .logh
	;~ pop eax
	;~ ret
;~ .logh:
	;~ add al, '0'
	;~ cmp al, '0' + 9
	;~ jle .p
	;~ add al, 7
;~ .p:
	;~ out 0xE9, al
	;~ ret